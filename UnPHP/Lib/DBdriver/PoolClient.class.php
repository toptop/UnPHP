<?php

/**
 * @desc		框架扩展：数据库操作类库|数据库资源池
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

//use PDO;
//use DBPoolClientInterface;
//use DBInterface;

class PoolClient implements DBPoolClientInterface
{

        protected $_host       = null;
        protected $_port       = null;
        protected $_dbname     = null;
        protected $_user       = null;
        protected $_password   = null;
        protected $_engineType = 'mysql';
        protected $_charset    = 'utf8';
        protected $_prefix     = '';

        /**
         *
         * @var DbInterface 
         */
        protected $_client = null;

        public function __construct($host, $port, $dbName, $user, $password)
        {
                $this->_host     = $host;
                $this->_port     = $port;
                $this->_dbname   = $dbName;
                $this->_user     = $user;
                $this->_password = $password;
        }

        public function setEngineType($engineType)
        {
                $this->_engineType = $engineType;
        }

        public function setCharset($charset)
        {
                $this->_charset = $charset;
        }

        public function setPrefix($prefix)
        {
                $this->_prefix = $prefix;
        }

        public function getPrefix(){
                return $this->_prefix;
        }

        public function __get($name)
        {
                $att = array('engineType', 'host', 'port', 'dbname', 'user', 'password', 'charset', 'prefix');
                if (in_array($name, $att))
                {
                        $name = '_' . $name;
                        return $this->$name;
                }
                return null;
        }

        /**
         * 
         * @return PdoEngine
         */
        public function conn()
        {
                if (null == $this->_client)
                {
                        switch ($this->_engineType)
                        {
                                case 'mysql':
                                case 'pgsql':
                                        $this->_client = new PdoEngine($this);
                                        break;
                                case 'mongodb':
                                        $this->_client = new MongoEngine($this);
                        }
                }
                return $this->_client;
        }

}
