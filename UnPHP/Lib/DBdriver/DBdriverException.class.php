<?php

/**
 * @desc		精简版Smarty扩展异常
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

use UnPHP\Exception\UnPHPException;
class DBdriverException extends UnPHPException
{
        //put your code here
                protected $code = 500020101;
}
