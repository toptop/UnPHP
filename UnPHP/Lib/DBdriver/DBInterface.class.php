<?php

/**
 * @desc		框架接口：规范---数据库（驱动）操作类
 *             （在一般性操作中，与mongodb通用）
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

interface DBInterface
{

    public function begin();

    public function commit();

    public function rollback();

    public function getPk($collectionName);

    public function sqlQuery($prepareSql, $bindParams);
    
    public function findOne($collectionName, $condition = array(), $options = array());
    
    public function findOneHash($collectionName, $condition = array(), $options = array());

    public function findAll($collectionName, $condition = array(), $options = array());
    
    public function findAllHash($collectionName, $condition = array(), $options = array());

    public function count($collectionName, $condition = array());
    
    public function countHash($collectionName, $condition = array(), $options = array());

    public function insert($collectionName, $new, $options = array());

    public function batchInsert($collectionName, $new, $options = array());

    public function remove($collectionName, $condition = array(), $options = array());

    public function update($collectionName, $condition, $new, $options = array());
    
    public function errorInfo();
    
}
