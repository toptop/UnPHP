<?php

/**
 * @desc		框架扩展：数据库操作类库|Pdo类型（关系型数据库）操作接口类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

use PDO;

//use DBInterface;

class PdoEngine implements DBInterface
{

        private $_isBegin         = false;
        private $_last_error_info = null;
        private $_conn            = null;
        private static $_error_handle = array();

        /**
         *
         * @var EngineWriteHandleInterface
         */
        private static $_write_after_handle = null;

        /**
         *
         * @var PoolClient 
         */
        private $_poolClient   = null;
        private $_engineCustom = null;
        private $_filed_type   = array(
            'int' => 'int',
        );

        public function __construct(PoolClient $poolClient)
        {
                $this->_poolClient = $poolClient;
        }

        private function _getPoolClient()
        {
                return $this->_poolClient;
        }

        protected function _getConn()
        {
                if (null === $this->_conn)
                {
                        $dsn = $this->_getPoolClient()->engineType . ":dbname=" . $this->_getPoolClient()->dbname . ";host=" . $this->_getPoolClient()->host;
                        if (null != $this->_getPoolClient()->port)
                        {
                                $dsn .=";port=" . $this->_getPoolClient()->port;
                        }
                        $this->_conn  = new PDO($dsn, $this->_getPoolClient()->user, $this->_getPoolClient()->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '" . $this->_getPoolClient()->charset . "'"));
                        $engine_class = 'Pdo' . ucfirst($this->_getPoolClient()->engineType);
                        switch ($engine_class)
                        {
                                case 'PdoMysql':
                                        $this->_engineCustom = new PdoMysql($this->_conn);
                                        break;
                        }
                }
                return $this->_conn;
        }

        protected function _prepare($sql)
        {
                Pool::traceSql($sql);
                return $this->_getConn()->prepare($sql);
        }

        protected function _getCollection($collectionName)
        {
                return $this->_getPoolClient()->prefix . $collectionName;
        }

        public static function setErrorHandle($k, $f)
        {
                self::$_error_handle[$k] = $f;
        }

        public static function getErrorHandle()
        {
                return self::$_error_handle;
        }

        public static function setWriteHandle(EngineWriteHandleInterface $handle)
        {
                self::$_write_after_handle = $handle;
        }

        /**
         * 
         * @return EngineWriteHandleInterface
         */
        public static function getWriteHandle()
        {
                return self::$_write_after_handle;
        }

        public function getPk($collectionName)
        {
                
        }

        public function errorInfo()
        {
                return $this->_last_error_info;
        }

        public function begin()
        {
                $this->_isBegin = true;
                return $this->_getConn()->beginTransaction();
        }

        public function isBegin()
        {
                return $this->_isBegin;
        }

        public function commit()
        {
                $this->_isBegin = false;
                return $this->_getConn()->commit();
        }

        public function rollback()
        {
                return $this->_getConn()->rollBack();
        }

        public function findOne($collectionName, $condition = array(), $options = array())
        {
                try
                {
                        $bind_params = array();
                        $where       = $this->_querySql($condition, $bind_params);
                        $filed       = $this->_getFields($options);
                        $order       = $this->_getOrder($options);
                        //$limit = $this->_getLimit($options);
                        $lock        = $this->_getLock($options);
                        $sql         = "SELECT " . $filed . " FROM " . $this->_getCollection($collectionName) . $where . $order . 'limit 1' . $lock;
                        $sth         = $this->_prepare($sql);
                        //var_dump($sql);exit;
                        foreach ($bind_params as $key => $v)
                        {
                                $value = $v;
                                if (is_array($v))
                                {
                                        $value = "(" . implode(",", $v) . ")";
                                }
                                $sth->bindValue($key, $value);
                        }
                        $sth->execute();
                        $rs = $sth->fetch(PDO::FETCH_ASSOC);

                        return $rs;
                }
                catch (Exception $exc)
                {
                        echo $exc->getTraceAsString();
                }
        }

        public function findOneHash($collectionName, $condition = array(), $options = array())
        {
                $bind_params = array();
                $where       = $this->_querySql($condition, $bind_params);
                $filed       = $this->_getFields($options);
                $order       = $this->_getOrder($options);
                //$limit = $this->_getLimit($options);
                $lock        = $this->_getLock($options);
                $sql         = "SELECT " . $filed . " FROM " . $this->_getCollection($collectionName) . $where . $order . 'limit 1' . $lock;
                $md5         = "";
                $md5 .= md5($sql);
                $bindValue   = array();
                foreach ($bind_params as $key => $v)
                {
                        $value = $v;
                        if (is_array($v))
                        {
                                $value = "(" . implode(",", $v) . ")";
                        }
                        $bindValue[$key] = $value;
                }
                $md5 .= md5(serialize($bindValue));
                return md5($md5);
        }
        
        public function sqlQueryAll($prepareSql, $bindParams)
        {
                $sth = $this->_prepare($prepareSql);
                foreach ($bindParams as $key => $value)
                {
                        $sth->bindValue($key, $value);
                }
                $sth->execute();
                $rs = $sth->fetchAll(PDO::FETCH_ASSOC);
                return $rs;
        }

        public function sqlQueryCount($prepareSql, $bindParams)
        {
                $sth = $this->_prepare($prepareSql);
                foreach ($bindParams as $key => $value)
                {
                        $sth->bindValue($key, $value);
                }
                $sth->execute();
                $rs = $sth->rowCount();
                return $rs;
        }

        public function sqlQuery($prepareSql, $bindParams)
        {
                $sth = $this->_prepare($prepareSql);
                foreach ($bindParams as $key => $value)
                {
                        $sth->bindValue($key, $value);
                }
                $sth->execute();
                $rs = $sth->fetch(PDO::FETCH_ASSOC);
                return $rs;
        }

        public function findAll($collectionName, $condition = array(), $options = array())
        {
                $bind_params = array();
                $where       = $this->_querySql($condition, $bind_params);
                $filed       = $this->_getFields($options);
                $order       = $this->_getOrder($options);
                $limit       = $this->_getLimit($options);
                $lock        = $this->_getLock($options);
                $sql         = "select " . $filed . " FROM " . $this->_getCollection($collectionName) . $where . $order . $limit . $lock;
                $sth         = $this->_prepare($sql);
                foreach ($bind_params as $key => $v)
                {
                        $value = $v;
                        if (is_array($v))
                        {
                                $value = "(" . implode(",", $v) . ")";
                        }
                        $sth->bindValue($key, $value);
                }
                //echo $sql;exit;
                $sth->execute();
                $rs = $sth->fetchAll(PDO::FETCH_ASSOC);
                return $rs;
        }

        public function findAllHash($collectionName, $condition = array(), $options = array())
        {
                $bind_params = array();
                $where       = $this->_querySql($condition, $bind_params);
                $filed       = $this->_getFields($options);
                $order       = $this->_getOrder($options);
                $limit       = $this->_getLimit($options);
                $lock        = $this->_getLock($options);
                $sql         = "select " . $filed . " FROM " . $this->_getCollection($collectionName) . $where . $order . $limit . $lock;
                $md5         = "";
                $md5 .= md5($sql);
                $bindValue   = array();
                foreach ($bind_params as $key => $v)
                {
                        $value = $v;
                        if (is_array($v))
                        {
                                $value = "(" . implode(",", $v) . ")";
                        }
                        $bindValue[$key] = $value;
                }
                $md5 .= md5(serialize($bindValue));
                return md5($md5);
        }

        public function count($collectionName, $condition = array())
        {
                $bind_params = array();
                $where       = $this->_querySql($condition, $bind_params);
                $filed       = 'count(*) as num';
                $sql         = "SELECT " . $filed . " FROM " . $this->_getCollection($collectionName) . $where;
                $sth         = $this->_prepare($sql);
                foreach ($bind_params as $key => $v)
                {
                        $value = $v;
                        if (is_array($v))
                        {
                                $value = "(" . implode(",", $v) . ")";
                        }
                        $sth->bindValue($key, $value);
                }
                $sth->execute();
                $rs = $sth->fetch(PDO::FETCH_ASSOC);
                return $rs['num'];
        }

        public function countHash($collectionName, $condition = array(), $options = array())
        {
                $bind_params = array();
                $where       = $this->_querySql($condition, $bind_params);
                $filed       = 'count(*) as num';
                $sql         = "SELECT " . $filed . " FROM " . $this->_getCollection($collectionName) . $where;
                $md5         = "";
                $md5 .= md5($sql);
                $bindValue   = array();
                foreach ($bind_params as $key => $v)
                {
                        $value = $v;
                        if (is_array($v))
                        {
                                $value = "(" . implode(",", $v) . ")";
                        }
                        $bindValue[$key] = $value;
                }
                $md5 .= md5(serialize($bindValue));
                return md5($md5);
        }

        public function insert($collectionName, $new, $options = array())
        {
                if (is_array($new) && !empty($new))
                {
                        $conn   = $this->_getConn();
                        $fileds = $this->_engineCustom->get_table_filed_type($this->_getCollection($collectionName));
                        $sql    = $this->_getInsertSql($this->_getCollection($collectionName), $fileds);
                        $sth    = $this->_prepare($sql);
                        $data   = array();
                        $rs     = array();
                        foreach ($fileds['field'] as $filed => $v)
                        {
                                if ($fileds['auto_key_id'] == $filed)
                                {
                                        continue;
                                }
                                $data[':' . $filed] = $this->get_field_value($v['type'], isset($new[$filed]) ? $new[$filed] : $v['default']);
                                $rs[$filed]         = $this->get_field_value($v['type'], isset($new[$filed]) ? $new[$filed] : $v['default']);
                        }
                        if ($sth->execute($data))
                        {
                                $insertId = $conn->lastInsertId();
                                if (null !== self::getWriteHandle())
                                {
                                        self::getWriteHandle()->handler('insert', $this->_getCollection($collectionName), $sql, $data);
                                }
                                return $insertId;
                        }
                        else
                        {
                                //错误操作句柄
                                $errorHandle = self::getErrorHandle();
                                if ($errorHandle)
                                {
                                        foreach ($errorHandle as $handle)
                                        {
                                                $errInfo = $sth->errorInfo();
                                                $handle($sql, $data,$errInfo[2]);
                                        }
                                }
                                return FALSE;
                        }
                }
                else
                {
                        return FALSE;
                }
        }

        public function batchInsert($collectionName, $new, $options = array())
        {
                if (is_array($new) && !empty($new))
                {
                        $fileds = $this->_engineCustom->get_table_filed_type($this->_getCollection($collectionName));
                        $sql    = $this->_getInsertSql($this->_getCollection($collectionName), $fileds, true);
                        $sth    = $this->_prepare($sql);
                        $data   = array();
                        $rs     = array();
                        foreach ($new as $row)
                        {
                                foreach ($fileds['field'] as $filed => $v)
                                {
                                        if ($fileds['auto_key_id'] == $filed)
                                        {
                                                continue;
                                        }
                                        $data[':' . $filed] = $this->get_field_value($v['type'], isset($row[$filed]) ? $row[$filed] : $v['default']);
                                        $rs[$filed]         = $this->get_field_value($v['type'], isset($row[$filed]) ? $row[$filed] : $v['default']);
                                }
                                if (!$sth->execute($data))
                                {
                                        //错误操作句柄
                                        $errorHandle = self::getErrorHandle();
                                        if ($errorHandle)
                                        {
                                                foreach ($errorHandle as $handle)
                                                {
                                                        $errInfo = $sth->errorInfo();
                                                        $handle($sql, $data,$errInfo[2]);
                                                }
                                        }
                                        return FALSE;
                                }
                                //操作成功，回调执行句柄
                                if (null !== self::getWriteHandle())
                                {
                                        self::getWriteHandle()->handler('insert', $this->_getCollection($collectionName), $sql, $data);
                                }
                        }
                        return true;
                }
                else
                {
                        return FALSE;
                }
        }

        public function remove($collectionName, $condition = array(), $options = array())
        {
                $bind_params = array();
                $where       = $this->_querySql($condition, $bind_params);
                $order       = $this->_getOrder($options);
                $limit       = $this->_getLimit($options);
                $sql         = "DELETE FROM " . $this->_getCollection($collectionName) . $where . $order . $limit;
                $sth         = $this->_prepare($sql);
                foreach ($bind_params as $key => $value)
                {
                        $sth->bindValue($key, $value);
                }
                if (!$sth->execute())
                {
                        //错误操作句柄
                        $errorHandle = self::getErrorHandle();
                        if ($errorHandle)
                        {
                                foreach ($errorHandle as $handle)
                                {
                                        $errInfo = $sth->errorInfo();
                                        $handle($sql, $bind_params,$errInfo[2]);
                                }
                        }
                        return false;
                }
                $colcount = $sth->rowCount();
                if (null !== self::getWriteHandle())
                {
                        self::getWriteHandle()->handler('remove', $this->_getCollection($collectionName), $sql, $data);
                }
                return $colcount;
        }

        public function update($collectionName, $condition, $new, $options = array())
        {
                if ($new)
                {
                        $bind_params = array();
                        $where       = $this->_querySql($condition, $bind_params);
                        $set         = "";
                        $key_str     = "set_";
                        $i           = 0;
                        if (isset($new['set']))
                        {
                                $setList = $new['set'];
                                foreach ($setList as $k => $v)
                                {
                                        $temp_key               = $key_str . $i++;
                                        $set .= $k . "=:" . $temp_key . ",";
                                        $bind_params[$temp_key] = $v;
                                }
                        }
                        if (isset($new['inc']))
                        {
                                $incList = $new['inc'];
                                foreach ($incList as $k => $v)
                                {
                                        $set .= $k . "=" . $k . "+" . $v . ",";
                                }
                        }
                        $set = substr($set, 0, -1);
                        $sql = "UPDATE " . $this->_getCollection($collectionName) . " SET " . $set . $where;
                        $sth = $this->_prepare($sql);
                        foreach ($bind_params as $key => $value)
                        {
                                $sth->bindValue($key, $value);
                        }
                        if (!$sth->execute())
                        {
                                //错误操作句柄
                                $errorHandle = self::getErrorHandle();
                                if ($errorHandle)
                                {
                                        foreach ($errorHandle as $handle)
                                        {
                                                $errInfo = $sth->errorInfo();
                                                $handle($sql, $bind_params,$errInfo[2]);
                                        }
                                }
                                return false;
                        }
                        $colcount = $sth->rowCount();
                        if (null !== self::getWriteHandle())
                        {
                                self::getWriteHandle()->handler('update', $this->_getCollection($collectionName), $sql, $data);
                        }
                        return $colcount;
                }
        }

        private function _getLock($options)
        {
                $lock = isset($options['lock']) && $options['lock'] == 1 ? " FOR UPDATE " : " ";
                return $lock;
        }

        private function _getFields($options)
        {
                $fields = isset($options['fields']) && count($options['fields']) > 0 ? implode(',', $options['fields']) : "*";
                return $fields;
        }

        private function _getOrder($options)
        {
                $order = "";
                if (isset($options['sort']) && is_array($options['sort']) && !empty($options['sort']))
                {
                        $order = " Order by ";
                        foreach ($options['sort'] as $key => $value)
                        {
                                if ($value == 1)
                                {
                                        $order .= $key . " ASC,";
                                }
                                else
                                {
                                        $order .= $key . " DESC,";
                                }
                        }
                        $order = substr($order, 0, -1) . " ";
                }
                return $order;
        }

        private function _getLimit($options)
        {
                $limits = "";
                if (isset($options['offset']) || isset($options['limit']))
                {
                        $limits = " Limit ";
                        if (isset($options['offset']))
                        {
                                $limits .= intval($options['offset']) . " ";
                        }
                        else
                        {
                                $limits .= " 0 ";
                        }
                        if (isset($options['limit']) && $options['limit'] > 0)
                        {
                                $limits .= "," . intval($options['limit']) . " ";
                        }
                }
                return $limits;
        }

        private function _querySql($condition = array(), &$bind_params = array())
        {
                try
                {
                        $where   = " WHERE 1=1 AND ";
                        $key_str = "where_";
                        $i       = 0;
                        if ($condition)
                        {
                                //and
                                if (isset($condition[ParseQuery::TAG_AND]))
                                {
                                        $andList = $condition[ParseQuery::TAG_AND];
                                        foreach ($andList as $k => $tagList)
                                        {
                                                foreach ($tagList as $tag => $vList)
                                                {
                                                        $sign = $this->sign_to_sql($tag);
                                                        if ($sign)
                                                        {
                                                                foreach ($vList as $v)
                                                                {
                                                                        $temp_key               = $key_str . $i++;
                                                                        $where .= $k . $sign . ':' . $temp_key . ' AND ';
                                                                        $bind_params[$temp_key] = $v;
                                                                }
                                                        }
                                                }
                                        }
                                }
                                
                                //or
                                if (isset($condition[ParseQuery::TAG_OR]))
                                {
                                        $orList = $condition[ParseQuery::TAG_OR];
                                        foreach ($orList as $k => $tagList)
                                        {
                                                foreach ($tagList as $tag => $vList)
                                                {
                                                        $sign = $this->sign_to_sql($tag);
                                                        if ($sign)
                                                        {
                                                                foreach ($vList as $v)
                                                                {
                                                                        $temp_key               = $key_str . $i++;
                                                                        $where .= $k . $sign . ':' . $temp_key . ' OR ';
                                                                        $bind_params[$temp_key] = $v;
                                                                }
                                                        }
                                                }
                                        }
                                }
                                
                                //in
                                if (isset($condition[ParseQuery::TAG_IN]))
                                {
                                        $inList = $condition[ParseQuery::TAG_IN];
                                        foreach ($inList as $k => $vList)
                                        {
                                                $temp_key = $key_str . $i++;
                                                $where .= $k . ' in (';
                                                foreach ($vList as $v)
                                                {
                                                        $temp_key               = $key_str . $i++;
                                                        $bind_params[$temp_key] = $v;
                                                        $where .= ':' . $temp_key . ',';
                                                }
                                                $where = substr($where, 0, -1) . ") AND ";
                                        }
                                }

                                //like
                                if (isset($condition[ParseQuery::TAG_LIKE]))
                                {
                                        $list = $condition[ParseQuery::TAG_LIKE];
                                        $sign = $this->sign_to_sql(ParseQuery::TAG_LIKE);
                                        foreach ($list as $k => $vList)
                                        {
                                                foreach($vList as $v){
                                                        $temp_key               = $key_str . $i++;
                                                        $bind_params[$temp_key] = '%'.$v.'%';
                                                        $where .= $k .' '. $sign . ' :' . $temp_key . ' AND ';
                                                }
                                        }
                                }
                        }
                        return substr($where, 0, -4);
                }
                catch (Exception $exc)
                {
                        echo $exc->getTraceAsString();
                }
        }

        private function _setLastErrorInfo($errorInfo)
        {
                $this->_last_error_info = $errorInfo;
                Pool::throwError($errorInfo);
        }

        private function sign_to_sql($str)
        {
                $rs = '';
                switch ($str)
                {
                        case ParseQuery::TAG_EQ:
                                $rs = '=';
                                break;
                        case ParseQuery::TAG_GT:
                                $rs = '>';
                                break;
                        case ParseQuery::TAG_LT:
                                $rs = '<';
                                break;
                        case ParseQuery::TAG_GTE:
                                $rs = '>=';
                                break;
                        case ParseQuery::TAG_LTE:
                                $rs = '<=';
                                break;
                        case ParseQuery::TAG_NE:
                                $rs = '<>';
                                break;
                        case ParseQuery::TAG_LIKE:
                                $rs = 'LIKE';
                                break;
                }
                if (empty($rs))
                        return false;
                return $rs;
        }

        private function get_field_value($type, $value)
        {
                $rs = null;
                switch ($type)
                {
                        case 'int':
                                $rs = intval($value);
                                break;
                        case 'float':
                                $rs = floatval($value);
                                break;
                        case 'string':
                                $rs = strval($value);
                                break;
                        default:
                                $rs = strval($value);
                                break;
                }
                return $rs;
        }

        private function _getInsertSql($collectionName, $fileds, $is_batch = false)
        {
                $insert_files  = "(";
                $insert_values = "(";
                foreach ($fileds['field'] as $k => $v)
                {
                        if ($fileds['auto_key_id'] == $k)
                        {
                                continue;
                        }
                        $insert_files .= $k . ',';
                        $insert_values .= ':' . $k . ',';
                }
                $insert_files  = substr($insert_files, 0, -1) . ') ';
                $insert_values = substr($insert_values, 0, -1) . ')';
                $sql           = $is_batch ? "INSERT INTO `{$collectionName}` " . $insert_files . 'values ' . $insert_values :
                        "INSERT INTO `{$collectionName}` " . $insert_files . 'value ' . $insert_values;
                return $sql;
        }

}
