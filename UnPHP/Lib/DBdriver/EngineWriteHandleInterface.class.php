<?php

/**
 * @desc		数据库引擎，写入操作
 *                                       回调方法接口
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

interface EngineWriteHandleInterface
{
        public function handler($action, $table, $sql, $data);
}
