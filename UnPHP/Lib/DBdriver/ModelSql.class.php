<?php

/**
 * @desc		数据操作接口基类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

class ModelSql extends CommonSql
{

        protected $_dbName = null;

        //protected $_conn_master = false;

        //protected $_table  = null;

        /**
         * 设置“私有接口”---调用的模块（服务端的模块）名称
         * @param type $dbName
         * @return ModelSql
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function __construct($dbName)
        {
                $this->_dbName = $dbName;
                return $this;
        }

        /**
         *
         * @return ModelSql
         */
        public function master(){
                $this->_conn_master = true;
                return $this;
        }

        public function getDbName()
        {
                return $this->_dbName;
        }

        /**
         * 返回一个“更新协助类”
         * @return ModelSqlParseCreate
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function createWriter()
        {
                $newdata = new ModelSqlParseCreate($this);
                return $newdata;
        }

        /**
         * 返回一个“查询解析帮助类”
         * @return ModelSqlParseQuery
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function createQuery()
        {
                $newdata = new ModelSqlParseQuery($this);
                return $newdata;
        }

        
        public function sqlQuery($prepareSql, $bindParams)
        {
                $dbName = $this->getDbName();
                if (empty($dbName))
                {
                        return FALSE;
                }
                return $this->_getPool($dbName)->conn()->sqlQuery($prepareSql, $bindParams);
        }

        public function sqlQueryAll($prepareSql, $bindParams)
        {
                $dbName = $this->getDbName();
                if (empty($dbName))
                {
                        return FALSE;
                }
                return $this->_getPool($dbName)->conn()->sqlQueryAll($prepareSql, $bindParams);
        }

        public function sqlQueryCount($prepareSql, $bindParams){
                $dbName = $this->getDbName();
                if (empty($dbName))
                {
                        return FALSE;
                }
                return $this->_getPool($dbName)->conn()->sqlQueryCount($prepareSql, $bindParams);
        }

        public function getPrefix(){
                $dbName = $this->getDbName();
                if (empty($dbName))
                {
                        return FALSE;
                }
                return $this->_getPool($dbName)->getPrefix();
        }

        /**
         * 数据表通用接口---查询多条结果集
         * @param queryParse $parse
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function findAll($table, ModelSqlParseQuery $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition       = $parse->getQueryCondition();
                $options         = array();
                $options['lock'] = $parse->getLock();
                $sort            = $parse->getQueryOrder();
                $fileds          = $parse->getQueryFileds();
                $offset          = $parse->getQueryOffset();
                $limit           = $parse->getQueryLimit();
                if (!empty($sort))
                {
                        $options['sort'] = $sort;
                }
                if (!empty($fileds))
                {
                        $options['fields'] = $fileds;
                }
                if (null !== $offset && null !== $limit)
                {
                        $options['offset'] = $offset;
                        $options['limit']  = $limit;
                }
                return $this->_getPool($dbName)
                                ->conn()
                                ->findAll($table, $condition, $options);
        }

        /**
         * 
         * @param queryParse $parse
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function findAllHash($table, ModelSqlParseQuery $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition       = $parse->getQueryCondition();
                $options         = array();
                $options['lock'] = $parse->getLock();
                $sort            = $parse->getQueryOrder();
                $fileds          = $parse->getQueryFileds();
                $offset          = $parse->getQueryOffset();
                $limit           = $parse->getQueryLimit();
                if (!empty($sort))
                {
                        $options['sort'] = $sort;
                }
                if (!empty($fileds))
                {
                        $options['fields'] = $fileds;
                }
                if (null !== $offset && null !== $limit)
                {
                        $options['offset'] = $offset;
                        $options['limit']  = $limit;
                }
                return $this->_getPool($dbName)
                                ->conn()
                                ->findAllHash($table, $condition, $options);
        }

        /**
         * 数据表通用接口---查询集合总数值
         * @param queryParse $parse
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function findCount($table, ModelSqlParseQuery $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition = $parse->getQueryCondition();
                return $this->_getPool($dbName)
                                ->conn()
                                ->count($table, $condition);
        }

        /**
         * @param queryParse $parse
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function findCountHash($table, ModelSqlParseQuery $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition = $parse->getQueryCondition();
                return $this->_getPool($dbName)
                                ->conn()
                                ->countHash($table, $condition);
        }

        /**
         * 数据表通用接口---查询一条结果集
         * @param queryParse $parse
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function findOne($table, ModelSqlParseQuery $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition       = $parse->getQueryCondition();
                $options         = array();
                $options['lock'] = $parse->getLock();
                $sort            = $parse->getQueryOrder();
                $fileds          = $parse->getQueryFileds();
                if (!empty($sort))
                {
                        $options['sort'] = $parse->getQueryOrder();
                }
                if (!empty($fileds))
                {
                        $options['fields'] = $parse->getQueryFileds();
                }
                return $this->_getPool($dbName)
                                ->conn()
                                ->findOne($table, $condition, $options);
        }

        /**
         * 
         * @param queryParse $parse
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function findOneHash($table, ModelSqlParseQuery $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition       = $parse->getQueryCondition();
                $options         = array();
                $options['lock'] = $parse->getLock();
                $sort            = $parse->getQueryOrder();
                $fileds          = $parse->getQueryFileds();
                if (!empty($sort))
                {
                        $options['sort'] = $parse->getQueryOrder();
                }
                if (!empty($fileds))
                {
                        $options['fields'] = $parse->getQueryFileds();
                }
                return $this->_getPool($dbName)
                                ->conn()
                                ->findOneHash($table, $condition, $options);
        }

        /**
         * 数据表通用接口---更新结果集
         * @param CreateData $new
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function update($table, ModelSqlParseCreate $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition = $parse->getQueryCondition();
                $new       = $parse->getUpdateData();
                return $p         = Pool::instance()->getWritePool($dbName)->conn()->update($table, $condition, $new);
        }

        /**
         * 数据表通用接口---插入一条数据到集合中
         * @param CreateData $new
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function insert($table, ModelSqlParseCreate $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $new = $parse->getNew();
                $p   = Pool::instance()->getWritePool($dbName);
                return $p->conn()->insert($table, $new);
        }

        /**
         * 数据表通用接口---插入一条数据到集合中
         * @param CreateData $new
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function batchInsert($table, $parse_list)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $new = array();
                foreach ($parse_list as $parse)
                {
                        $new[] = $parse->getNew();
                }
                $p = Pool::instance()->getWritePool($dbName);
                return $p->conn()->batchInsert($table, $new);
        }

        /**
         * 数据表通用接口---删除数据
         * @param type $table
         * @param \system\lib\DBdriver\ModelSqlParseCreate $parse
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function remove($table, ModelSqlParseCreate $parse)
        {
                $dbName = $this->getDbName();
                if (empty($table) || empty($dbName))
                {
                        return FALSE;
                }
                $condition = $parse->getQueryCondition();
                return Pool::instance()->getWritePool($dbName)->conn()->remove($table, $condition);
        }

        public function errorInfo()
        {
                $dbName = $this->getDbName();
                return Pool::instance()->getWritePool($dbName)->conn()->errorInfo();
        }

        /**
         * @param $dbName
         * @return PoolClient
         */
        private function _getPool($dbName){
                $getPool = $this->_conn_master ? "getWritePool" : "getReadPool";
                return Pool::instance()->$getPool($dbName);
        }

}
