<?php

/**
 * @desc		框架接口：规范---数据库连接池（“一主多从”类型的数据库）
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Lib\DBdriver;

interface DBPoolInterface
{
    
    public function regReadPool($code,  DBPoolClientInterface $client);
    
    public function regWritePool($code,  DBPoolClientInterface $client);
    
    public function getReadPool($code);
    
    public function getWritePool($code);
            
}
