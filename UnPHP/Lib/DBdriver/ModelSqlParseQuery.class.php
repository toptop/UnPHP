<?php

/**
 * @desc		查询解析类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

class ModelSqlParseQuery extends ParseQuery
{

        /**
         *
         * @var ModelSql 
         */
        private $_model = null;

        public function __construct(ModelSql $model)
        {
                $this->_model = $model;
        }

        public function findAll($table)
        {
                return $this->_model->findAll($table, $this);
        }

        public function findAllHash($table)
        {
                return $this->_model->findAllHash($table, $this);
        }

        public function setDbName($dbName)
        {
                $this->_model->setDbName($dbName);
                return $this;
        }

        public function findOne($table)
        {
                return $this->_model->findOne($table, $this);
        }

        public function findOneHash($table)
        {
                return $this->_model->findOneHash($table, $this);
        }

        public function findCount($table)
        {
                return $this->_model->findCount($table, $this);
        }

        public function findCountHash($table)
        {
                return $this->_model->findCountHash($table, $this);
        }

}
