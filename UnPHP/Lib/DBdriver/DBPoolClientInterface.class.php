<?php

/**
 * @desc		框架接口：规范---数据库连接句柄
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

interface DBPoolClientInterface
{
    public function conn();
}
