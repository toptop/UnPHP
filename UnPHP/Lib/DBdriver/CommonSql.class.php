<?php

/**
 * @desc		数据操作接口基类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

abstract class CommonSql
{

        protected $_conn_master = false;

        abstract public function getDbName();

        /**
         * 开启事务
         * 此方法只有“关系型”数据库才有效，对于mongodb无效
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function begin()
        {
                $p = Pool::instance()->regBegin($this->getDbName());
                if (false === $p)
                {
                        return false;
                }
                $this->_conn_master = true;
                return $p->conn()->begin();
        }

        /**
         * 提交事务
         * 此方法只有“关系型”数据库才有效，对于mongodb无效
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function commit()
        {
                $p = Pool::instance()->getBegin($this->getDbName());
                //var_dump($p);exit;
                if (false === $p)
                {
                        return false;
                }
                Pool::instance()->closeBegin($this->getDbName());
                return $p->conn()->commit();
        }

        /**
         * 事务回滚
         * 此方法只有“关系型”数据库才有效，对于mongodb无效
         * @return boolean
         * @author		liuxiang <unphp@qq.com>
         * @date		2015-04-03
         */
        public function rollback()
        {
                $p = Pool::instance()->getBegin($this->getDbName());
                if (false === $p)
                {
                        return false;
                }
                //Pool::instance()->closeBegin($this->getDbName());
                return $p->conn()->rollback();
        }

        public function isBegin()
        {
                $p = Pool::instance()->getBegin($this->getDbName());
                if (false === $p)
                {
                        return false;
                }
                return $p->conn()->isBegin();
        }

        public function traceSql()
        {
                return Pool::instance()->getSql();
        }
}
