<?php

/**
 * @desc		查询解析类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

class ModelSqlParseCreate extends ParseQuery
{

        private $_model      = null;
        private $_new        = array();
        private $_update_set = array();
        private $_update_inc = array();
        private $_fileds     = null;

        public function __construct(ModelSql $model)
        {
                $this->_model = $model;
        }

        public function addNew($filed, $value)
        {
                $this->_new[$filed] = $value;
                return $this;
        }

        public function adds($list)
        {
                $this->_new = array_merge($this->_new, $list);
                return $this;
        }

        public function getNew()
        {
                return $this->_new;
        }

        public function setUpdate($filed, $value)
        {
                $this->_update_set[$filed] = $value;
                return $this;
        }

        public function setUpdates($arr)
        {
                $this->_update_set = array_merge($this->_update_set, $arr);
                return $this;
        }

        public function setInc($filed, $value)
        {
                $this->_update_inc[$filed] = $value;
                return $this;
        }

        public function getUpdateData()
        {
                return array('set' => $this->_update_set, 'inc' => $this->_update_inc);
        }

        public function getFileds()
        {
                if (null === $this->_fileds)
                {
                        $model         = $this->getModel();
                        $this->_fileds = $model->fileds();
                }
                return $this->_fileds;
        }

        public function getModel()
        {
                return $this->_model;
        }

        public function update($table)
        {
                return $this->_model->update($table, $this);
        }

        public function insert($table)
        {
                return $this->_model->insert($table, $this);
        }

        public function batchInsert($table)
        {
                return $this->_model->insert($table, $this);
        }

        public function remove($table)
        {
                return $this->_model->remove($table, $this);
        }

}
