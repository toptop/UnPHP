<?php

/**
 * @desc		数据操作接口基类（查询拼接器）
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-27
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\DBdriver;

class ParseQuery
{

        const TAG_OR  = '$or';
        const TAG_AND = '$and';
        const TAG_EQ  = '$eq';    // 查询条件 “=”
        const TAG_GT  = '$gt';     // 查询条件 “>”
        const TAG_GTE = '$gte'; // 查询条件 “>=”
        const TAG_LT  = '$lt';      // 查询条件 “<”
        const TAG_LTE = '$lte';   // 查询条件 “<=”
        const TAG_NE  = '$ne';   // 查询条件 “<>”即“!=”
        const TAG_IN  = '$in';   // 查询条件 “in”
        const TAG_LIKE  = '$like';   // 查询条件 “like”

        protected $_queryCondition = array();
        protected $_queryOrder     = array();
        protected $_queryFields    = array();
        protected $_queryOffset    = null;
        protected $_queryLimit     = null;
        protected $_lock           = 0;

        public function getQueryCondition()
        {
                return $this->_queryCondition;
        }

        public function getQueryFileds()
        {
                return array_unique($this->_queryFields);
        }

        public function getQueryOffset()
        {
                return $this->_queryOffset;
        }

        public function getQueryLimit()
        {
                return $this->_queryLimit;
        }

        public function getQueryOrder()
        {
                return $this->_queryOrder;
        }

        public function getLock()
        {
                return $this->_lock;
        }

        public function setAnd($k, $v, $logical = ParseQuery::TAG_EQ)
        {
                $this->_queryCondition[self::TAG_AND][$k][$logical][] = $v;
                return $this;
        }

        public function setOr($k, $v, $logical = ParseQuery::TAG_EQ)
        {
                $this->_queryCondition[self::TAG_OR][$k][$logical][] = $v;
                return $this;
        }

        public function setQueryLike($k,$v){
                $this->_queryCondition[self::TAG_LIKE][$k][] = $v;
                return $this;
        }

        public function setIn($k, $v)
        {
                if (!isset($this->_queryCondition[self::TAG_IN][$k]))
                {
                        $this->_queryCondition[self::TAG_IN][$k] = array();
                }
                if (is_array($v))
                {
                        if (!empty($v))
                        {
                                $this->_queryCondition[self::TAG_IN][$k] = array_merge($this->_queryCondition[self::TAG_IN][$k], $v);
                        }
                }
                else
                {
                        $list                                    = explode(',', $v);
                        $this->_queryCondition[self::TAG_IN][$k] = array_merge($this->_queryCondition[self::TAG_IN][$k], $list);
                }
                $this->_queryCondition[self::TAG_IN][$k] = array_unique($this->_queryCondition[self::TAG_IN][$k]);
                return $this;
        }

        public function setQueryFileds($k)
        {
                if (is_array($k))
                {
                        $this->_queryFields = array_merge($this->_queryFields, $k);
                }
                else
                {
                        $list               = explode(',', $k);
                        $this->_queryFields = array_merge($this->_queryFields, $list);
                }
                return $this;
        }

        public function setQueryLimit($limit, $offset = 0)
        {
                $this->_queryOffset = $offset;
                $this->_queryLimit  = $limit;
                return $this;
        }

        public function setAsc($k)
        {

                $this->_queryOrder[$k] = 1;

                return $this;
        }

        public function setLock()
        {
                $this->_lock = 1;
                return $this;
        }

        public function setDesc($k)
        {
                $this->_queryOrder[$k] = -1;

                return $this;
        }

        public function addNew($filed, $value)
        {
                return $this;
        }

        public function setUpdate($filed, $value)
        {
                return $this;
        }

        public function setUpdates($arr)
        {
                return $this;
        }

        public function setInc($filed, $value)
        {
                return $this;
        }

        public function findAll($table)
        {
                
        }

        public function findAllHash($table)
        {
                
        }

        /**
         * 
         * @param type $table
         * @return Model
         */
        public function findOne($table)
        {
                
        }

        public function findOneHash($table)
        {
                
        }

        public function findCount($table)
        {
                
        }

        public function findCountHash($table)
        {
                
        }

        public function update($table)
        {
                
        }

        public function insert($table)
        {
                
        }

        public function remove($table)
        {
                
        }

}
