<?php

/**
 * @desc		一些积累下来的函数库
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2016-06-06
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

function in($data, $force = false, $htmlspecialchar = true)
{
	if ( is_string($data) )
	{
		if ( $htmlspecialchar == true )
		{
			$data = trim(htmlspecialchars($data)); // 防止被挂马，跨站攻击
		}
		if ( ($force == true) || (! get_magic_quotes_gpc()) )
		{
			$data = addslashes($data); // 防止sql注入
		}
		return $data;
	}
	else if ( is_array($data) ) // 如果是数组采用递归过滤
	{
		foreach ($data as $key => $value)
		{
			$data[$key] = in($value, $force);
		}
		return $data;
	}
	else
	{
		return $data;
	}
}

function limitStart($page,&$size,$sizeMin=10,$sizeMax=50){
	$page = $page < 1 ? 1 : $page;
	$size = ($size < $sizeMin ? 10 : $size) > $sizeMax ? $sizeMax : $size;
	$start       = ($page - 1) * $size;
	return $start;
}

// 用来还原字符串和字符串数组，把已经转义的字符还原回来
function out($data)
{
	if ( is_string($data) )
	{
		return $data = stripslashes($data);
	}
	else if ( is_array($data) ) // 如果是数组采用递归过滤
	{
		foreach ($data as $key => $value)
		{
			$data[$key] = out($value);
		}
		return $data;
	}
	else
	{
		return $data;
	}
}

// 文本输入
function text_in($str)
{
	$str = strip_tags($str, '<br>');
	$str = str_replace(" ", "&nbsp;", $str);
	$str = str_replace("\n", "<br>", $str);
	if ( ! get_magic_quotes_gpc() )
	{
		$str = addslashes($str);
	}
	return $str;
}

// 文本输出
function text_out($str)
{
	$str = str_replace("&nbsp;", " ", $str);
	$str = str_replace("<br>", "\n", $str);
	$str = stripslashes($str);
	return $str;
}

// html代码输入
function html_in($str)
{
	$search = array(
		"'<script[^>]*?>.*?</script>'si", // 去掉 javascript
		"'<iframe[^>]*?>.*?</iframe>'si" // 去掉iframe
		);
	$replace = array(
		"",
		""
	);
	$str = @preg_replace($search, $replace, $str);
	$str = htmlspecialchars($str);
	if ( ! get_magic_quotes_gpc() )
	{
		$str = addslashes($str);
	}
	return $str;
}

// html代码输出
function html_out($str)
{
	if ( function_exists('htmlspecialchars_decode') )
	{
		$str = htmlspecialchars_decode($str);
	}
	else
	{
		$str = html_entity_decode($str);
	}
	
	$str = stripslashes($str);
	return $str;
}

// 获取客户端IP地址
function get_client_ip()
{
	if ( getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown") )
	{
		$ip = getenv("HTTP_CLIENT_IP");
	}
	else if ( getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown") )
	{
		$ip = getenv("HTTP_X_FORWARDED_FOR");
	}
	else if ( getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown") )
	{
		$ip = getenv("REMOTE_ADDR");
	}
	else if ( isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown") )
	{
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	else
	{
		$ip = "unknown";
	}
	
	return $ip;
}

// 中文字符串截取
function msubstr($str, $start = 0, $length, $charset = "utf-8", $suffix = true)
{
	switch ($charset)
	{
		case 'utf-8':
			$char_len = 3;
			break;
		case 'UTF8':
			$char_len = 3;
			break;
		default:
			$char_len = 2;
	}
	// 小于指定长度，直接返回
	if ( strlen($str) <= ($length * $char_len) )
	{
		return $str;
	}
	if ( function_exists("mb_substr") )
	{
		$slice = mb_substr($str, $start, $length, $charset);
	}
	else if ( function_exists('iconv_substr') )
	{
		$slice = iconv_substr($str, $start, $length, $charset);
	}
	else
	{
		$re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
		$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
		$re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
		$re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
		preg_match_all($re[$charset], $str, $match);
		$slice = join("", array_slice($match[0], $start, $length));
	}
	if ( $suffix )
	{
		return $slice . "…";
	}
	return $slice;
}

// 检查是否是正确的邮箱地址，是则返回true，否则返回false
function is_email($user_email)
{
	$chars = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
	if ( strpos($user_email, '@') !== false && strpos($user_email, '.') !== false )
	{
		if ( preg_match($chars, $user_email) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

// 检查字符串是否是UTF8编码,是返回true,否则返回false
function is_utf8($string)
{
	return preg_match('%^(?:
		 [\x09\x0A\x0D\x20-\x7E]            # ASCII
	   | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
	   |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
	   | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
	   |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
	   |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
	   | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
	   |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
   )*$%xs', $string);
}

// 自动转换字符集 支持数组转换
function auto_charset($fContents, $from = 'gbk', $to = 'utf-8')
{
	$from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
	$to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
	if ( strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && ! is_string($fContents)) )
	{
		// 如果编码相同或者非字符串标量则不转换
		return $fContents;
	}
	if ( is_string($fContents) )
	{
		if ( function_exists('mb_convert_encoding') )
		{
			return mb_convert_encoding($fContents, $to, $from);
		}
		elseif ( function_exists('iconv') )
		{
			return iconv($from, $to, $fContents);
		}
		else
		{
			return $fContents;
		}
	}
	elseif ( is_array($fContents) )
	{
		foreach ($fContents as $key => $val)
		{
			$_key = auto_charset($key, $from, $to);
			$fContents[$_key] = auto_charset($val, $from, $to);
			if ( $key != $_key )
			{
				unset($fContents[$key]);
			}
		}
		return $fContents;
	}
	else
	{
		return $fContents;
	}
}

// 获取微秒时间，常用于计算程序的运行时间
function utime()
{
	list ($usec, $sec) = explode(" ", microtime());
	return ((float) $usec + (float) $sec);
}

// 生成唯一的值
function getuniqid()
{
	return md5(uniqid(rand(), true));
}

// 加密函数，可用decode()函数解密，$data：待加密的字符串或数组；$key：密钥；$expire 过期时间
function encode($data, $key = '', $expire = 0)
{
	$string = serialize($data);
	$ckey_length = 4;
	$key = md5($key);
	$keya = md5(substr($key, 0, 16));
	$keyb = md5(substr($key, 16, 16));
	$keyc = substr(md5(microtime()), - $ckey_length);
	
	$cryptkey = $keya . md5($keya . $keyc);
	$key_length = strlen($cryptkey);
	
	$string = sprintf('%010d', $expire ? $expire + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
	$string_length = strlen($string);
	$result = '';
	$box = range(0, 255);
	
	$rndkey = array();
	for ( $i = 0; $i <= 255; $i ++ )
	{
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}
	
	for ( $j = $i = 0; $i < 256; $i ++ )
	{
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}
	
	for ( $a = $j = $i = 0; $i < $string_length; $i ++ )
	{
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}
	return $keyc . str_replace('=', '', base64_encode($result));
}

// encode之后的解密函数，$string待解密的字符串，$key，密钥
function decode($string, $key = '')
{
	$ckey_length = 4;
	$key = md5($key);
	$keya = md5(substr($key, 0, 16));
	$keyb = md5(substr($key, 16, 16));
	$keyc = substr($string, 0, $ckey_length);
	
	$cryptkey = $keya . md5($keya . $keyc);
	$key_length = strlen($cryptkey);
	
	$string = base64_decode(substr($string, $ckey_length));
	$string_length = strlen($string);
	
	$result = '';
	$box = range(0, 255);
	
	$rndkey = array();
	for ( $i = 0; $i <= 255; $i ++ )
	{
		$rndkey[$i] = ord($cryptkey[$i % $key_length]);
	}
	
	for ( $j = $i = 0; $i < 256; $i ++ )
	{
		$j = ($j + $box[$i] + $rndkey[$i]) % 256;
		$tmp = $box[$i];
		$box[$i] = $box[$j];
		$box[$j] = $tmp;
	}
	
	for ( $a = $j = $i = 0; $i < $string_length; $i ++ )
	{
		$a = ($a + 1) % 256;
		$j = ($j + $box[$a]) % 256;
		$tmp = $box[$a];
		$box[$a] = $box[$j];
		$box[$j] = $tmp;
		$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
	}
	if ( (substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16) )
	{
		return unserialize(substr($result, 26));
	}
	else
	{
		return '';
	}
}

// 遍历删除目录和目录下所有文件
function del_dir($dir)
{
	if ( ! is_dir($dir) )
	{
		return false;
	}
	$handle = opendir($dir);
	while (($file = readdir($handle)) !== false)
	{
		if ( $file != "." && $file != ".." )
		{
			is_dir("$dir/$file") ? del_dir("$dir/$file") : @unlink("$dir/$file");
		}
	}
	if ( readdir($handle) == false )
	{
		closedir($handle);
		@rmdir($dir);
	}
}

// 获取请求头部信息，兼容nginx
if ( ! function_exists('getallheaders') )
{

	function getallheaders()
	{
		$headers = '';
		foreach ($_SERVER as $name => $value)
		{
			if ( substr($name, 0, 5) == 'HTTP_' )
			{
				$headers[(str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))))] = $value;
			}
		}
		return $headers;
	}
}

/*
 * 取出指定二维数组中指定 key 的数据集 @param $array 数组数据 @param $keyid 要取得的 key @param $mode 取出的模式: 'string': 字符串 'array': 数组 'sum': 同 key 元素之和
 */
function get_array(&$array, $keyid, $mode = 'array')
{
	if ( ! is_array($array) )
	{
		return false;
	}
	$arr_tmp = array();
	foreach ($array as $key => $val)
	{
		if ( ! in_array($val[$keyid], $arr_tmp) )
		{
			$arr_tmp[] = $val[$keyid];
		}
	}
	return ($mode == 'array') ? $arr_tmp : implode("','", $arr_tmp);
}

/*
 * 根据指定的数组和关键字生成一维数组
 */
function create_array($array, $key_val, $key_name, $key_name2 = '')
{
	if ( is_array($array) )
	{
		$result = array();
		foreach ($array as $val)
		{
			if ( ! empty($key_name2) && ! empty($val[$key_name2]) )
			{
				$result[$val[$key_val]] = $val[$key_name] . " (" . $val[$key_name2] . ")";
				continue;
			}
			$result[$val[$key_val]] = $val[$key_name];
		}
		return $result;
	}
	return false;
}

/*
 * 计算文件大小 @param int $size 文件大小
 */
if ( ! function_exists("get_realsize") )
{

	function get_realsize($size = 0)
	{
		$kb = 1024; // Kilobyte
		$mb = 1024 * $kb; // Megabyte
		$gb = 1024 * $mb; // Gigabyte
		$tb = 1024 * $gb; // Terabyte
		if ( $size < $kb )
		{
			return $size . " B";
		}
		else if ( $size < $mb )
		{
			return round($size / $kb, 2) . " KB";
		}
		else if ( $size < $gb )
		{
			return round($size / $mb, 2) . " MB";
		}
		else if ( $size < $tb )
		{
			return round($size / $gb, 2) . " GB";
		}
		else
		{
			return round($size / $tb, 2) . " TB";
		}
	}
}

// 生成随机数
function generate_random_char($length = 8)
{
	// 密码字符集，可任意添加你需要的字符
	$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	$temp = '';
	for ( $i = 0; $i < $length; $i ++ )
	{
		// 这里提供两种字符获取方式
		// 第一种是使用 substr 截取$chars中的任意一位字符；
		// 第二种是取字符数组 $chars 的任意元素
		// $password .= substr($chars, mt_rand(0, strlen($chars) – 1), 1);
		$temp .= $chars[mt_rand(0, strlen($chars) - 1)];
	}
	return $temp;
}

// 把json對像轉為數組
function object_to_array($obj)
{
	$_arr = is_object($obj) ? get_object_vars($obj) : $obj;
	if ( is_array($_arr) )
	{
		foreach ($_arr as $key => $val)
		{
			$val = (is_array($val) || is_object($val)) ? object_to_array($val) : $val;
			$arr[$key] = $val;
		}
	}
	return $arr;
}

/**
 * 中文反序列化
 */
function mb_unserialize($serial_str)
{
	$out = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $serial_str);
	return unserialize($out);
}

/**
 * 判断是否json格式
 */
function is_json($string)
{
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * 解析json串
 * 
 * @param type $json_str        	
 * @return type
 *
 */
function analyJson($json_str)
{
	$out_arr = array();
	
	if ( is_string($json_str) )
	{
		$json_str = str_replace('\\', '', $json_str);
		preg_match('/{.*}/', $json_str, $out_arr);
	}
	else
	{
		return FALSE;
	}
	
	if ( ! empty($out_arr) )
	{
		$result = json_decode($out_arr[0], TRUE);
	}
	else
	{
		return FALSE;
	}
	return $result;
}

/**
 * 求出2个数组的不同值
 */
function array_diffs($array1, $array2)
{
	return array_merge(array_diff($array1, $array2), array_diff($array2, $array1));
}

// 检测手机号码
function checkMobile($str)
{
	$pattern = "/^(13|15|18|17|14)\d{9}$/"; // 手机号码
	
	if ( preg_match($pattern, $str) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// 检测手机号码
function checkTel($str)
{
	$pattern = "/^([0-9]{3,4}-)?[0-9]{7,8}$/"; // 座机号码
	
	if ( preg_match($pattern, $str) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

    function checkTelphone($str)
    {
        $isPhone = "/^([0-9]{3,4}-)?[0-9]{7,8}$/"; // 座机号码
        $isMob="/^(13|15|18|17|14)\d{9}$/"; // 手机号码
        if( preg_match($isPhone, $str) || preg_match($isMob, $str))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

// 检查邮箱地址
function checkEmail($str)
{
	$chars = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
	if ( strpos($str, '@') !== false && strpos($str, '.') !== false )
	{
		if ( preg_match($chars, $str) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

// 检查网址是否有效
function checkUrl($str)
{
	$pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
	
	if ( preg_match($pattern, $str) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// 检查是否是number
function is_number($str)
{
	if ( substr($str, 0, 1) == "-" )
	{
		$str = substr($str, 1);
	}
	$length = strlen($str);
	$i = 0;
	for (; $i < $length; ++ $i )
	{
		$ascii_value = ord(substr($str, $i, 1));
		if ( 48 <= $ascii_value && $ascii_value <= 57 )
		{
			continue;
		}
		return FALSE;
	}
	if ( $str != "0" )
	{
		$str = intval($str);
		if ( $str == 0 )
		{
			return FALSE;
		}
	}
	return TRUE;
}

// 检查是否是data
function is_date($str)
{
	$YEAR = "";
	$MONTH = "";
	$DAY = "";
	$len = strlen($str);
	$offset = 0;
	$i = strpos($str, "-", $offset);
	$YEAR = substr($str, $offset, $i - $offset);
	$offset = $i + 1;
	if ( $len < $offset )
	{
		return FALSE;
	}
	if ( $i )
	{
		$i = strpos($str, "-", $offset);
		$MONTH = substr($str, $offset, $i - $offset);
		$offset = $i + 1;
		if ( $len < $offset )
		{
			return FALSE;
		}
		if ( $i )
		{
			$DAY = substr($str, $offset, $len - $offset);
		}
	}
	if ( $YEAR == "" || $MONTH == "" || $DAY == "" )
	{
		return FALSE;
	}
	if ( ! checkdate(intval($MONTH), intval($DAY), intval($YEAR)) )
	{
		return FALSE;
	}
	return TRUE;
}

// 检查是否time
function is_time($str)
{
	$TEMP = "";
	$HOUR = "";
	$MIN = "";
	$SEC = "";
	$TEMP = strtok($str, ":");
	$HOUR = $TEMP;
	if ( $HOUR == "" || 24 <= $HOUR || $HOUR < 0 || ! is_number($HOUR) )
	{
		return FALSE;
	}
	$TEMP = strtok(":");
	$MIN = $TEMP;
	if ( $MIN == "" || 60 <= $MIN || $MIN < 0 || ! is_number($MIN) )
	{
		return FALSE;
	}
	$TEMP = strtok(":");
	$SEC = $TEMP;
	if ( $SEC == "" || 60 <= $SEC || $SEC < 0 || ! is_number($SEC) )
	{
		return FALSE;
	}
	return TRUE;
}

// 检查是否datatime
function is_date_time($DATE_TIME_STR)
{
	if ( $DATE_TIME_STR == NULL || strlen($DATE_TIME_STR) == 0 )
	{
		return FALSE;
	}
	$DATE_TIME_ARRY = explode(" ", $DATE_TIME_STR);
	if ( is_date($DATE_TIME_ARRY[0]) && is_time($DATE_TIME_ARRY[1]) )
	{
		return TRUE;
	}
	return FALSE;
}

/**
 * 根据文件流获取文件信息
 * 
 * @author robben
 * @param string $stream        	
 */
function getfiletype($stream)
{
	$bin = substr($stream, 0, 2); // 截取2字节
	$str_info = @unpack("C2chars", $bin);
	$type_code = intval($str_info['chars1'] . $str_info['chars2']);
	$file_type = '';
	switch ($type_code)
	{
		case 7790:
			$file_type = 'exe';
			break;
		case 7784:
			$file_type = 'midi';
			break;
		case 8075:
			$file_type = 'zip';
			break;
		case 8297:
			$file_type = 'rar';
			break;
		case 255216:
			$file_type = 'jpg';
			break;
		case 7173:
			$file_type = 'gif';
			break;
		case 6677:
			$file_type = 'bmp';
			break;
		case 13780:
			$file_type = 'png';
			break;
	}
	
	return '.' . $file_type;
}

/**
 * 验证图片上传类型
 * 
 * @author robben
 * @param string $file_type        	
 * @return boolean
 */
function valid_image_type($file_type)
{
	$type = array(
		".gif",
		".jpg",
		".jpeg",
		".png",
		".bmp"
	);
	if ( in_array(strtolower($file_type), $type) )
	{
		return true;
	}
	return false;
}

/**
 * 创建文件目录
 * 
 * @author robben
 * @param string $dir        	
 * @param string $mode        	
 */
function create_directory($dir, $mode = 0777)
{
	if ( ! $dir )
	{
		return 0;
	}
	$dir = str_replace("\\", "/", $dir);
	$mdir = "";
	foreach (explode("/", $dir) as $val)
	{
		$mdir .= $val . "/";
		if ( $val == ".." || $val == "." )
		{
			continue;
		}
		if ( ! file_exists($mdir) )
		{
			if ( @mkdir($mdir) )
			{
				chmod($mdir, $mode);
			}
		}
	}
	return true;
}

/**
 * 生成图片的缩略图
 * 
 * @param string $soc_file
 *        	原文件
 * @param string $des_file
 *        	缩略文件
 * @param int $k_w        	
 * @param int $k_h        	
 */
function create_thumb($soc_file, $des_file, $k_w, $k_h)
{
	list ($soc_w, $soc_h, $type) = @getimagesize($soc_file); // 获取图片信息
	                                                        
	// 获取图片资源
	switch ($type)
	{
		case 1:
			$srcImg = imagecreatefromgif($soc_file);
			break;
		case 2:
			$srcImg = imagecreatefromjpeg($soc_file);
			break;
		case 3:
			$srcImg = imagecreatefrompng($soc_file);
			break;
		default:
			die("暂不支持该文件格式，请用图片处理软件将图片转换为GIF、JPG、PNG格式。");
	}
	
	// 设置图片大小
	$des_w = $soc_w; // 将原图片的宽度给数组中的$des_w
	$des_h = $soc_h; // 将原图片的高度给数组中的$des_h
	
	if ( $k_w < $soc_w )
	{
		$des_w = $k_w; // 缩放的宽度如果比原图小才重新设置宽度
	}
	if ( $k_h < $soc_h )
	{
		$des_h = $k_h; // 缩放的高度如果比原图小才重新设置高度
	}
	if ( $soc_w * $des_w > $soc_h * $des_h )
	{
		$des_h = round($soc_h * $des_w / $soc_w);
	}
	else
	{
		$des_w = round($soc_w * $des_h / $soc_h);
	}
	
	// 获取新的图片资源
	$newImg = imagecreatetruecolor($des_w, $des_h);
	$otsc = imagecolortransparent($srcImg); // 将某个颜色定义为透明色
	if ( $otsc >= 0 && $otsc < imagecolorstotal($srcImg) ) // 取得一幅图像的调色板中颜色的数目
	{
		$transparentcolor = imagecolorsforindex($srcImg, $otsc); // 取得某索引的颜色
		$newtransparentcolor = imagecolorallocate($newImg, $transparentcolor['red'], $transparentcolor['green'], $transparentcolor['blue']);
		imagefill($newImg, 0, 0, $newtransparentcolor);
		imagecolortransparent($newImg, $newtransparentcolor);
	}
	imagecopyresized($newImg, $srcImg, 0, 0, 0, 0, $des_w, $des_h, $soc_w, $soc_h);
	imagedestroy($srcImg);
	
	// 生成图片
	switch ($type)
	{
		case 1:
			imagegif($newImg, $des_file, 100);
			break;
		case 2:
			imagejpeg($newImg, $des_file, 100);
			break;
		case 3:
			imagepng($newImg, $des_file, 9);
			break;
	}
	imagedestroy($newImg);
}

/**
 * 输出各种类型的数据，调试程序时打印数据使用。
 * 
 * @param
 *        	mixed	参数：可以是一个或多个任意变量或值
 */
function p()
{
	$args = func_get_args(); // 获取多个参数
	if ( count($args) < 1 )
	{
		echo "请传入参数";
		return;
	}
	
	echo '<div style="width:100%;text-align:left"><pre>';
	// 多个参数循环输出
	foreach ($args as $arg)
	{
		if ( is_array($arg) )
		{
			print_r($arg);
			echo '<br>';
		}
		else if ( is_string($arg) )
		{
			echo $arg . '<br>';
		}
		else
		{
			var_dump($arg);
			echo '<br>';
		}
	}
	echo '</pre></div>';
}

function checkString($data, $data_len = null)
{
    if ((empty($data) && $data != '0') || !is_string($data))
    {
        return FALSE;
    }
    if (!empty($data_len))
    {
        if(mb_strlen($data,'gb2312') > $data_len)
        {
                return FALSE;
        }
    }
    return TRUE;
}

function checkInt($data, $data_len = null)
{
    $data = strval($data);
    if (!is_number($data) || empty($data) || $data < 1)
    {
        return FALSE;
    }
    if (!empty($data_len))
    {
        if(mb_strlen($data,'gb2312') > $data_len)
        {
                return FALSE;
        }
    }
    return TRUE;
}

function checkDouble($data, $data_len = null)
{
    if (!is_double($data) || empty($data) || $data < 0)
    {
        return FALSE;
    }
    if (!empty($data_len))
    {
        if(mb_strlen($data,'gb2312') > $data_len)
        {
                return FALSE;
        }
    }
    return TRUE;
}

/**
 * 密码规则检测
 * @param	string $user_pwd 用户密码
 * @return 	booble
 */
function isPwd($user_pwd)
{
    if (preg_match('/^[0-9a-z]{6,22}$/i',$user_pwd))
    {
        return true;
    }
    else
    {
        return false;
        }
}

if (!function_exists('array_column'))
{

        function array_column($input, $columnKey, $indexKey = NULL)
        {
                $columnKeyIsNumber = (is_numeric($columnKey)) ? TRUE : FALSE;
                $indexKeyIsNull    = (is_null($indexKey)) ? TRUE : FALSE;
                $indexKeyIsNumber  = (is_numeric($indexKey)) ? TRUE : FALSE;
                $result            = array();

                foreach ((array) $input AS $key => $row)
                {
                        if ($columnKeyIsNumber)
                        {
                                $tmp = array_slice($row, $columnKey, 1);
                                $tmp = (is_array($tmp) && !empty($tmp)) ? current($tmp) : NULL;
                        }
                        else
                        {
                                $tmp = isset($row[$columnKey]) ? $row[$columnKey] : NULL;
                        }
                        if (!$indexKeyIsNull)
                        {
                                if ($indexKeyIsNumber)
                                {
                                        $key = array_slice($row, $indexKey, 1);
                                        $key = (is_array($key) && !empty($key)) ? current($key) : NULL;
                                        $key = is_null($key) ? 0 : $key;
                                }
                                else
                                {
                                        $key = isset($row[$indexKey]) ? $row[$indexKey] : 0;
                                }
                        }

                        $result[$key] = $tmp;
                }

                return $result;
        }

}

        /**
        * 根据出生年月计算年龄
        * @param	string 出生年月
        * @return 	booble
        */
       function age($birthday)
       {
               //$age = date('Y', time()) - date('Y', strtotime($birthday)) - 1;
               $age = date('Y', time()) - date('Y', $birthday);
               if (date('m', time()) == date('m', $birthday))
               {
                       if (date('d', time()) >= date('d', $birthday))
                       {
                               $age++;
                       }
               }
               elseif (date('m', time()) > date('m', $birthday))
               {
                       $age++;
               }

               $age = $age < 0 ? 0 : $age;

               return $age;
       }

		function floorMoney($money)
		{
			return floor(floatval($money)*10)/10;
		}

		function hidMobile($mobile){
				return substr($mobile, 0, 3) . "****" . substr($mobile, 7);
		}
                
        function getCombinationToString($arr, $m) 
        {  
                if ($m ==1)
                {
                        return $arr;
                }
                $result = array();

                $tmpArr = $arr;
                unset($tmpArr[0]);
                $ret = getCombinationToString(array_values($tmpArr), ($m - 1), $result);
                for ($i = 0; $i < count($arr); $i++)
                {
                        $s = $arr[$i];
                        //$ret = getCombinationToString(array_values($tmpArr), ($m - 1), $result);
                        foreach ($ret as $row)
                        {
                                $result[] = $s . $row;
                        }
                        array_shift($ret);
                }

                return $result;
        }  
        
        function getcombinarybynum($arr, $num, $t = array())
        {
                if ($num == 0)
                {

                        return array($t);
                }
                $r = array();
                for ($i = 0, $l = count($arr); $i <= $l - $num; $i++)
                {
                        $tmp = getcombinarybynum(array_slice($arr, $i + 1, $l, false), $num - 1, array_merge($t, array($arr[$i])));
                        $r = array_merge($r, $tmp);
                }
                return $r;
        }
