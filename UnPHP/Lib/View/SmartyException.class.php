<?php

/**
 * @desc		精简版Smarty扩展异常
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Lib\View;

use UnPHP\Exception\UnPHPException;
class SmartyException extends UnPHPException
{

        //put your code here
        protected $code = 2002;

}
