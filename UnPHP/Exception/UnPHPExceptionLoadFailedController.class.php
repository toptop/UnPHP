<?php

/**
 * @desc		 异常类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Exception;

class UnPHPExceptionLoadFailedController extends UnPHPException
{

        protected $code = 500010106;

}
