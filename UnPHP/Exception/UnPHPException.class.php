<?php

/**
 * @desc		 异常类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Exception;

use App;
use Exception;
use UnPHP\Core\Dispatcher;

class UnPHPException extends Exception
{

        protected $message;
        //protected $code;
        private $previous;

        public function __construct($message, $code = 0, $previous = null)
        {
                parent::__construct($message, $code, $previous);
                App::app()->getDispatcher()->getRequest()->setException($this);
        }

}
