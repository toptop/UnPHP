<?php

/**
 * @desc		请求默认处理类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Core;

class RequestHttp extends RequestAbstract
{

        public function getLanguage()
        {
                
        }

        public function getQuery($name,$default=null)
        {
                $v = isset($_GET[$name]) ? $_GET[$name] : $default;
                return $v;
        }
        
        public function getQuerys()
        {
                return $_GET;
        }

        public function getPost($name,$default=null)
        {
                $v = isset($_POST[$name]) ? $_POST[$name] : $default;
                return $v;
        }
        
        public function getPosts()
        {
                return $_POST;
        }

        public function getEnv($name = NULL)
        {
                
        }

        public function getServer($name = NULL)
        {
                if (empty($name))
                {
                        return $_SERVER;
                }
                else
                {
                        return $_SERVER[$name];
                }
        }

        public function getCookie($name = NULL)
        {
                if (empty($name))
                {
                        return $_COOKIE;
                }
                else
                {
                        return $_COOKIE[$name];
                }
        }

        public function getFiles($name = NULL)
        {
                if (empty($name))
                {
                        return $_FILES;
                }
                else
                {
                        return $_FILES[$name];
                }
        }

        public function isGet()
        {
                $rs = 'GET' === $this->_method ? true : false;
                return $rs;
        }

        public function isPost()
        {
                $rs = 'POST' === $this->_method ? true : false;
                return $rs;
        }

        public function isHead()
        {
                
        }

        public function isXmlHttpRequest()
        {
                
        }

        public function isPut()
        {
                
        }

        public function isDelete()
        {
                
        }

        public function isOption()
        {
                
        }

        public function isCli()
        {
                
        }

}
