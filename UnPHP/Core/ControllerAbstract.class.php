<?php

/**
 * @desc		 控制器基类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Core;
use App;
use Library\pub\PapaCoreServerClient;
use Library\pub\Client;

abstract class ControllerAbstract
{

        protected $_dispatcher = null;
        protected $_response;
        protected $_view;
        public $actions = array();

        public final function __construct(Dispatcher $dispatcher)
        {
                $this->_dispatcher = $dispatcher;
                //$this->init();
        }

        public function init(){
        }

        

        public function getModuleName()
        {
                return $this->_dispatcher->getRequest()->getModuleName();
        }
        
        public function getControllerName(){
                return $this->_dispatcher->getRequest()->getControllerName();
        }
        
        public function getActionName(){
                return $this->_dispatcher->getRequest()->getActionName();
        }

        /**
         * 
         * @return RequestAbstract
         */
        public function getRequest()
        {
                return $this->_dispatcher->getRequest();
        }

        /**
         * 
         * @return ViewInterface
         */
        public function getView()
        {
                return $this->_dispatcher->getView();
        }
        
        //格式
        //status 1 成功 -1失败
        //msg 信息
        //data 返回的数据
        public function json($value)
        {
                echo json_encode($value);
                exit;
        }
        
        //页面挑战，暂时重定向
        public function redirect($url = "/")
        {
            header("Location:$url");
            exit;
        }
        
}
