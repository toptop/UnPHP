<?php

/**
 * @desc		视图类接口
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Core;

Interface ViewInterface{
        
        public function init($conf = array());

        public function render($view_path, $tpl_vars = NULL);

        public function display($view_path, $tpl_vars = NULL);

        public function assign($name, $value);

        public function setScriptPath($view_directory);

        public function getScriptPath();
        
}