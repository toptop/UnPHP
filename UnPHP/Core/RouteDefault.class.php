<?php

/**
 * @desc		默认的“路由协议”
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2014-03-05
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Core;

use App;
class RouteDefault implements RouteInterface {

        /**
         * 默认路由： /m/c/a/params1/1/params2/2/...
         * @author Xiao Tangren  <unphp@qq.com>
         * @data 2014-03-05
         * @param type $request
         * @return boolean
         */
        public function route(RequestAbstract $request) {
                $rs = FALSE;
                $match = array();
                $base_url = $request->getServer('REQUEST_URI');
                $selfPramas = stripos($base_url, '?');
                $base_url = $selfPramas ? substr($base_url, 0, $selfPramas) : $base_url;
                $base_url = trim($base_url, '/\\');
                $match = explode("/", $base_url);
                $n = count($match);
                $m = null;
                $c = null;
                $a = null;
                //var_dump($n);exit;
                //var_dump($match);exit;
                switch ($n) {
                        case 0:
                                $m = $request->getDefaultModule();
                                $c = $request->getDefaultController();
                                $a = $request->getDefaultAction();
                                $rs = TRUE;
                                break;
                        case 1:
                                if ($base_url==""){
                                        $m = $request->getDefaultModule();
                                        $c = $request->getDefaultController();
                                        $a = $request->getDefaultAction();
                                        $rs = TRUE;
                                        break;
                                }
                                if (in_array($match[0], App::app()->getModules())) {
                                        $m = $match[0];
                                        $c = $request->getDefaultController();
                                        $a = $request->getDefaultAction();
                                }
                                else {
                                        $m = $request->getDefaultModule();
                                        $c = $match[0];
                                        $a = $request->getDefaultAction();
                                }
                                $rs = TRUE;
                                break;
                        case 2:
                                if (in_array($match[0], App::app()->getModules())) {
                                        if ($match[0] === $request->getDefaultModule())
                                        {
                                                $m = $request->getDefaultModule();
                                                $c = $match[0];
                                                $a = $match[1];
                                        }
                                        else
                                        {
                                                $m = $match[0];
                                                $c = $match[1];
                                                $a = $request->getDefaultAction();
                                        }
                                        $rs = TRUE;
                                        break;
                                }
                                else {
                                        $m = $request->getDefaultModule();
                                        $c = $match[0];
                                        $a = $match[1];
                                        $rs = TRUE;
                                        break;
                                }
                        default:
                                if (in_array($match[0], App::app()->getModules()))
                                {
                                        if ($match[0] === $request->getDefaultModule())
                                        {
                                                $m = $request->getDefaultModule();
                                                $c = $match[1];
                                                $a = $match[2];
                                                for ($i = 3; $i < $n; $i+=2)
                                                {
                                                        $request->setParam($match[$i - 1], $match[$i]);
                                                }
                                        }
                                        else
                                        {
                                                $m = $match[0];
                                                $c = $match[1];
                                                $a = $match[2];
                                                for ($i = 4; $i < $n; $i+=2)
                                                {
                                                        $request->setParam($match[$i - 1], $match[$i]);
                                                }
                                        }
                                }
                                else {
                                        $m = $request->getDefaultModule();
                                        $c = $match[0];
                                        $a = $match[1];
                                        for ($i = 3; $i < $n; $i+=2) {
                                                $request->setParam($match[$i - 1], $match[$i]);
                                        }
                                }
                                $rs = TRUE;
                                break;
                }
                $request->setModuleName($m);
                $request->setControllerName($c);
                $request->setActionName($a);
                //var_dump(trim($base_url, '/\\'),$n,$m,$c,$a);exit;
                return $rs;
        }

        public function createUrl($module_controllers_action, $params = array()) {
                $url = "";
                $module_controllers_action = trim($module_controllers_action);
                if (!empty($module_controllers_action)) {
                        $temp = explode("/", trim($module_controllers_action, '/\\'));
                        switch (count($temp)) {
                                case 1:
                                        $url = $this->urlParams($temp[0], $params);
                                        break;
                                case 2:
                                        $url = $this->urlParams($temp[0] . '/' . $temp[1], $params);
                                        break;
                                case 3:
                                default:
                                        $url = $this->urlParams($temp[0] . '/' . $temp[1] . '/' . $temp[2], $params, TRUE);
                                        break;
                        }
                }
                else {
                        $url = $this->urlParams($url, $params);
                }
                return $url;
        }

        private function urlParams($url, $params, $b = false) {
                if ($b) {
                        $url .= '/';
                        if (!empty($params)) {
                                foreach ($params as $key => $value) {
                                        $url .= $key . '/' . $value . '/';
                                }
                        }
                }
                else {
                        $url .= '?';
                        if (!empty($params)) {
                                foreach ($params as $key => $value) {
                                        $url .= $key . '=' . $value . '&';
                                }
                        }
                }
                $url = substr($url, 0, -1);
                return $url;
        }

}
