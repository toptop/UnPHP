<?php

/**
 * @desc		 类自动加载机制---抽象类
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2015-05-16
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Core;

class LoadAbstract {

    protected $common_library = null;
    protected $app_library = null;
    protected $unphpPath = null;

    public function __construct($unphpPath, $appLibrary, $commonLibrary = null) {
        $this->unphpPath = $unphpPath;
        $this->common_library = $commonLibrary;
        $this->app_library = $appLibrary;
    }

}

/**
 * @desc		 类自动加载机制---接口
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2015-05-16
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
interface Load {

    public function autoLoad($className);
}
