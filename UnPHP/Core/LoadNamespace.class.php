<?php

/**
 * @desc		 类自动加载规则---命名空间
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2015-05-16
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */

namespace UnPHP\Core;

use Exception;
use UnPHP\Exception\UnPHPExceptionAutoLoad;

class LoadNamespace extends LoadAbstract implements Load
{

        public function autoLoad($className)
        {
                if (class_exists($className,false))
                {
                        return true;
                }
                // 优先加载应用库文件
                if ($this->load($this->app_library, $className))
                {
                        return true;
                }
                // 加载公共库文件
                if (!empty($this->common_library))
                {
                        foreach ($this->common_library as $library)
                        {
                                if ($this->load($library, $className))
                                {
                                        return true;
                                }
                        }
                }
                // 最后考虑加载框架内置库文件
                if ($this->load(rtrim($this->unphpPath, "\\/") . DIRECTORY_SEPARATOR . '../', $className))
                {
                        return true;
                }
                  try
                  {
//                        $className = str_replace("\\/", DIRECTORY_SEPARATOR, $className);
//                        $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
//                        if (false === strpos($className, DIRECTORY_SEPARATOR))
//                        {
//                                $className = __NAMESPACE__ . DIRECTORY_SEPARATOR . $className;
//                        }
//                        $path = rtrim($this->unphpPath, "\\/") . DIRECTORY_SEPARATOR . '../';
//                        $file1 = rtrim($path, "\\/") . DIRECTORY_SEPARATOR . $className . '.class.php';
//                         $file2 = rtrim($path, "\\/") . DIRECTORY_SEPARATOR . ltrim(str_replace('\\', '/', $className), "\\/") . '.class.php';
//                        var_dump($file1, $file2);
//                        exit;
//                          include APPLICATION_PATH."/Library/Ext/Alipay/request/AlipayTradePayRequest.php";
//                          return true;
                          throw new UnPHPExceptionAutoLoad('Auto load :' . $className . ',the  class not find!');
                  }
                  catch (Exception $exc)
                  {
                          echo $exc->getTraceAsString();
                          exit;
                  }

                //throw new Exception($className . " not find!");
                return false;
        }

        private function load($path, $className)
        {
                $className = str_replace("\\/", DIRECTORY_SEPARATOR, $className);
                $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
                if(false===strpos($className, DIRECTORY_SEPARATOR)){
                        $className = __NAMESPACE__.DIRECTORY_SEPARATOR.$className;
                }
               
                $file1 = rtrim($path, "\\/") . DIRECTORY_SEPARATOR . $className . '.class.php';
                //echo $file1."<br>";
                if (file_exists($file1))
                {
                        require $file1;
                        return TRUE;
                }
                $file2 = rtrim($path, "\\/") . DIRECTORY_SEPARATOR . ltrim(str_replace('\\', '/', $className), "\\/") . '.class.php';
                if (file_exists($file2))
                {
                        require $file2;
                        return TRUE;
                }
                return false;
        }

}
