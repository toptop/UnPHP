<?php

/**
 * @desc		 框架默认的类自动加载规则
 * ---------------------------------------------------------------------
 * @author	unphp <unphp@qq.com>
 * @date		2015-05-16
 * @copyright	UnPHP 1.1
 * ---------------------------------------------------------------------
 */
namespace UnPHP\Core;

class LoadDefault extends LoadAbstract implements Load
{

        public function autoLoad($className)
        {
                $temp_list = explode("_", $className);
                $after = "";
                for ($i = 0; $i < count($temp_list) - 1; $i++)
                {
                        $after .= '/' . $temp_list[$i];
                }
                $className = $temp_list[count($temp_list) - 1];
                // 优先加载应用库文件
                $appClass = $this->app_library . $after . '/' . $className . '.php';
                if (file_exists($appClass))
                {
                        include $appClass;
                        return;
                }
                // 加载公共库文件
                if (!empty($this->common_library))
                {
                        foreach ($this->common_library as $library)
                        {
                                $commonClass = $library . $after . '/' . $className . '.php';
                                if (file_exists($commonClass))
                                {
                                        include $commonClass;
                                        return;
                                }
                        }
                }
                // 最后考虑加载框架内置库文件
                $unphpClass = $this->unphpPath . $after . '/' . $className . '.php';
                if (file_exists($unphpClass))
                {
                        include $unphpClass;
                        return;
                }
        }

}
